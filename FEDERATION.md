Federation
==========

The ActivityPub implementation in this software strives to be compliant to the core spec where possible, while offering a range of services and features which normally aren't provided by ActivityPub projects.

Supported activities:

- `Follow(Actor)`, `Accept(Follow)`, `Reject(Follow)`, `Undo(Follow)`, `Undo(Accept/Follow)`.
- `Join(Group)`, `Accept(Join)`, `Reject(Join)`, `Leave(Group)`
- `Create(Note|Article|Question|Page|Document|Image|Video|Audio)`, `Update(Note|Article|Question|Page|Document|Image|Video|Audio)`, `Delete(Note|Article|Question|Page|Document|Image|Video|Audio)`.
- `Like(Note|Article|Question|Page|Document|Image|Video|Audio)`, `Undo(Like)`.
- `Dislike(Note|Article|Question|Page|Document|Image|Video|Audio)`,`Undo(Dislike)`.
- `Flag(Note|Article|Question|Page|Document|Image|Video|Audio)`.
- `Block(Note|Article|Question|Page|Document|Image|Video|Audio)`.
- `Invite(Event)`, `Accept(Invite)`, `Reject(Invite)`, `TentativeAccept(Invite)`, `TentativeReject(Invite)`, `Update(Event)`, `Delete(Event)`.
- `Arrive(target: Place)`, `Leave(target: Place)`
- `Arrive(Place|Note+location)`, `Leave(Place|Note+location)`.
- `Announce(Note)`, `Undo(Announce)`.
- `Update(Actor)`, `Move(Actor)`, `Delete(Actor)`.

Many other activity combinations we've seen in the wild are partially supported. If you're looking for something specific which is not in this list, please ask (https://fediversity.site/channel/streams). 

## Fediverse FEPs


- FEP-e232: Object links (aka quotedPosts)
- FEP-8b32: Object Integrity Proofs
- FEP-c390: Identity Proofs
- FEP-fb2a: Actor metadata
- FEP-612d: Identifying ActivityPub Objects through DNS
- FEP-2c59: Discovery of a Webfinger address from an ActivityPub actor
- FEP-61cf: The OpenWebAuth Protocol
- FEP-ef61: Portable Objects

Please see [Conversation Containers](https://fediversity.site/help/develop/en/Containers), which builds on the following 2 FEPs. The same mechanism may be used for groups.
- FEP-400e: Publicly-appendable ActivityPub collections
- FEP-7888: Demystifying the context property

Nomadic Identity: work in progress 2024-07-03
- FEP-4adb: Dereferencing identifiers with webfinger

Planned support:
- FEP-d556: Server-Level Actor Discovery Using WebFinger

## Fediverse FEPs we will not ever support

- ~~FEP-f1d5: Nodeinfo in FediverseSoftware~~ Take your trackers and MAUs and stick them where the sun don't shine.
- ~~FEP-eb48: Hashtags~~ Hashtags were once called "folksonomy"; created by **people** for their own needs. We support multiword hashtags and hashtags with emojis and other special characters, because people asked for them. We will not be removing this support. 
- ~~FEP-a5c5: Web Syndication Methods~~ Since you don't know if a provider will support Atom or RSS or both or neither, use Link discovery, which is a well established technology -- rather than depending on named route components. 


## C2S

This project supports ActivityPub C2S. You may authenticate with HTTP basic-auth, OAuth2, or OpenWebAuth. There is no media upload endpoint since the (deprecated) specification of that service has no workarounds for working in memory-restricted environments and most mobile phone photos exceed PHP's default upload size limits. 

## Client search interface

Please see [Federated Search](https://fediversity.site/help/develop/en/Federated_Search)

If public access is allowed to the content search interface (a site security setting), clients may search the content of public messages or tags and are returned an ActivityStreams Collection of search results. When authenticated via OpenWebAuth, the search results may contain their own content or private content which they are permitted to access.  

The URL endpoints for site search are:

  https://example.com/search?search=banana

  https://example.com/search?tag=banana

The URL endpoints for individual channel search are:

  https://example.com/search/username?search=banana

  https://example.com/search/username?tag=banana

Search permission is indicated by the 'canSearch' attribute of the actor record. This is an array of actors or groups or access lists that are allowed to search the given channel. 
    
## Direct Messages

Direct Messages (DM) are differentiated from other private messaging using the nomad:directMessage flag (boolean). This is compatible with the same facility provided by other projects in other namespaces and is not prefixed within activities so that these may potentially be aggregated.

## Events

Events and RSVP are supported per AS-vocabulary with the exception that a Create/Event is not transmitted. Invite/Event is the primary method of sharing events. For compatibility with some legacy applications, RSVP responses of Accept/Event, Reject/Event, TentativeAccept/Event and TentativeReject/Event are accepted as valid RSVP activities. By default, we send Accept/{Invite/Event} (and other RSVP responses) per AS-vocabulary. Events with no timezone (e.g. "all day events" or holidays) are sent with no 'Z' on the event times per RFC3339 Section 4.3. All event times are in UTC and timezone adjusted events are transmitted using Zulu time 'yyyy-mm-ddThh:mm:ssZ'. Event descriptions are sent using 'summary' and accepted using summary, name, and content in order of existence. These are converted internally to plaintext if they contain HTML. 

If 'location' contains coordinates, a map will typically be generated when rendered. 


## Nomadic Identity

Nomadic identity describes a mechanism where the data stored by your social network can be mirrored to multiple servers in near realtime and any of these servers can be used at any time should your "primary" server be unavailable (temporarily or permanently). The mechanisms for creating nomadic accounts and maintaining mirrors are currently not supported by ActivityPub but are available via the Nomad protocol. However, the instance information is provided to ActivityPub services which wish to support it in a more seamless manner. Otherwise DMs (for example) may need to be sent to all actor instances to ensure the nomadic actor receives them. There are many other examples. 

The actor record for nomadic accounts contains a 'copiedTo' property which is otherwise identical to Mastodon's 'movedTo' property. ActivityPub services wishing to support compatibility with this mechanism should ensure that each nomadic instance is "folded" into a single fediverse identity so that messages from the same actor across different instances are recognised as being the same author. Additionally, any posts sent to a nomadic account should be sent to all instances and private data should be made accessible to all actor instances. This includes historical data and posts as new instances can be created at any time.  

## Groups

Groups may be public or private. The initial thread starting post to a group is sent using a DM to the group and should be the only DM recipient. This helps preserve the sanctity of private groups and is a posting method available to most ActivityPub software. Bang tags (!groupname) may also be used if supported. @mentions may be used for public groups (only). The initial post will be converted to an embedded post authored by the group Actor (and attributed to the original Actor) and resent to all members. In the future this will be provided as Add/Remove from the group collection -- see also [Conversation Containers](https://fediversity.site/help/develop/en/Containers). Followups and replies to group posts use normal federation methods. 

The actor type is 'Group' and can be followed using Follow/Group *or* Join/Group, and unfollowed by Undo/Follow *or* Leave/Group. Other software will see both a Follow/Group activity and a Join/Group activity when somebody using this software joins a group. Consume whatever works for your software and ignore the other. 

Announce activities are sent alongside comments for the benefit of microblog projects. 

## Comments

This project provides permission control and moderation of comments. By default comments are only accepted from existing connections. This can be changed by the individual. The `canReply` field may be used as an indicator of general permission to comment by consuming software. This is an array of actors or collections which may be permitted to reply. If it is empty, replies will not be accepted. An `endDate` on a published Note (/Article/etc.) without a corresponding `startDate` indicates a time limit on replies. They will be rejected after that datetime.  


## Expiring content

Activity objects may include an 'expires' field; after which time they are removed. The removal occurs with a federated Delete, but this is a best faith effort. We automatically delete any local objects we receive with an 'expires' field after it expires regardless of whether we receive a Delete activity. The expiration is specified as an ISO8601 date/time. 

## Private Media

Private media MAY be accessed using OCAP or OpenWebAuth. Bearcaps are supported but not currently generated. 


## Permission System

The Nomad permission system has years of historical use and is different than the typical ActivityPub project. A Follow activity by an actor on this project typically means the actor on this project has changed the permissions associated with the followee. This could one of 15-30 different access controls and does not by itself indicate a follower relationship. It could provide permission for the target actor to upload to file storage for example. Future work will make the precise permission set available for access over ActivityPub to consuming applications. This is currently in progress as of 29-May-2024.  


## Delivery model

See [Conversation Containers](https://fediversity.site/help/develop/en/Containers)

## Content

Content may be rich multi-media and renders nicely as HTML. Multicode is used and stored internally. Multicode is html, markdown, and bbcode. The multicode interpreter allows you to switch between these or combine them at will. Content is not obviously length-limited and authors MAY use up to the storage maximum of 24MB. In practice markup conversion limits the effective length to around 200KB and the default "maximum length of imported content" from other sites is 200KB. This can be changed on a per-site basis but this is rare. A Note may contain one or more images, other media, and links. HTML purification primarily removes javascript and iframes and allows most legitimate tags and CSS styles through. Inline images are also added as attachments for the benefit of Mastodon (which strips most HTML), but remain in the HTML source. When importing content from other sites, if the content contains an image attachment, the content is scanned to see if a link (a) or (img) tag containing that image is already present in the HTML. If not, an image tag is added inline to the end of the incoming content. Multiple images are supported using this mechanism.

Mastodon 'summary' does not invoke any special handling so 'summary' can be used for its intended purpose as a content summary. Mastodon 'sensitive' is honoured and results in invoking whatever mechanisms the user has selected to deal with this type of content. By default images are obscured and are 'click to view'. Sensitive text is not treated specially, but may be obscured using the NSFW plugin or filtered per connection based on string match, tags, patterns, languages, or other criteria.


    
## Edits

Edited posts and comments are sent with Update/Note and an 'updated' timestamp along with the original 'published' timestamp.

Actor records contain an 'updated' timestamp of the last "profile change" (typically displayName). An updated timestamp is also provided on the icon representing the profile photo, so if cached, this only needs to be updated on a consumer service if it has changed. 

## Announce

Announce and relay activities use two mechanisms. As well as the Announce activity, a new message is generated with an embedded rendering of the shared content as the message content. This message may (should) contain additional commentary in order to comply with the Fair Use provisions of copyright law. The other reason is our use of comment permissions. Comments to Announce activities are sent to the author (who typically accepts comments only from connections). Comment to embedded forwards are sent to the sender. This difference in behaviour allows groups to work correctly in the presence of comment permissions.


## Emoji Reactions

We consider a reply message containing exactly one emoji and no other text or markup to be an emoji reaction. We indicate this state internally on receipt but currently do nothing to identify it specifically to downstream recipients.

## Mastodon Custom Emojis

Mastodon Custom Emojis are only supported for post content and are not considered as emoji reactions. Display names and message titles (ActivityStreams "name" field) are considered text only fields and embedded images (the mechanism behind custom emojis) are not supported in these locations.


## Mentions and hashtags and private mentions

By default the mention format is '@Display Name', but other options are available, such as '@username' and both '@Display Name (username)'. Mentions may also contain an exclamation character, for example '@!Display Name'.  This indicates a Direct or private message to 'Display Name' and post addressing/privacy are altered accordingly. Group posts may be triggered by a so-called "bang tag"; for example !groupname. This **only** sends the post to 'groupname', and all other recipients are excluded. This syntax is recommended for private groups for that reason. All incoming and outgoing content to your stream is re-written to display mentions in your chosen style. This mechanism may need to be adopted by other projects to provide consistency as there are strong feelings on both sides of the debate about which form should be prevalent in the fediverse.

Hashtags may contain multiple words with space between them. To create one of these, surround the worlds in double quotes - for instance #"Multiple Word Hashtag". The quotes are removed when viewing. Many, but not all fediverse platforms will display these correctly. 

## (Mastodon) Comment Notifications

Our projects send comment notifications if somebody replies to a post you either created or have previously interacted with in some way. They also are able to send a "mention" notification if you were mentioned in the post. This differs from Mastodon which does not appear to support comment notifications at all and only provides mention notifications. For this reason, Mastodon users don't typically get notified unless the author they are replying to is mentioned in the post. We provide this mention in the 'tag' field of the generated Activity, but normally don't include it in the message body, as we don't actually require mentions that were created for the sole purpose of triggering a comment notification.

## Conversation Completion

(2021-04-17) It's easy to fetch missing pieces of a conversation going "upstream", but there is no agreed-on method to fetch a complete conversation from the viewpoint of the origin actor and upstream fetching only provides a single conversation branch, rather than the entire tree. We provide both 'context' and 'audience' as a URL to a collection containing the entire conversation (all known branches) as seen by its creator. This requires special treatment and is very similar to ostatus:conversation in that if context is present, it needs to be replicated in conversation descendants. We still support ostatus:conversation but usage is deprecated. We do not use 'replies' to achieve the same purposes because 'replies' only applies to direct descendants at any point in the conversation tree. 

## Site Actors

(2021-08-25) An actor record of type 'Service' is now available from an ActivityStreams fetch of the domain root. This was discussed recently in the Socialhub as it may open some novel applications which involve communication with sites and site administators; and also provides a simple ActivityPub centric method for discovering very basic information about a site that doesn't involve platform-centric APIs. At present this is just a skeleton which will be filled in as we better define the ways we see it being used.
