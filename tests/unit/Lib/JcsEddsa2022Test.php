<?php

namespace Code\Tests\Unit\Lib;

use Code\Lib\Activity;
use Code\Lib\JcsEddsa2022;
use Code\Lib\PConfig;
use Code\Tests\Unit\UnitTestCase;

class JcsEddsa2022Test extends UnitTestCase
{

    public function testVerifyFromSpec()
    {
        $publicKey = 'z6MkrJVnaZkeFzdQyMZu1cgjg7k1pZZ6pvBQ7XJPt4swbTQ2';
        $privateKey = 'z3u2en7t5LR2WtQH5PfFqMqwVHBeXouLzo6haApm8XHqvjxq';

        $document = '{
  "@context": [
    "https://www.w3.org/ns/credentials/v2",
    "https://www.w3.org/ns/credentials/examples/v2"
  ],
  "id": "urn:uuid:58172aac-d8ba-11ed-83dd-0b3aef56cc33",
  "type": [
    "VerifiableCredential",
    "AlumniCredential"
  ],
  "name": "Alumni Credential",
  "description": "A minimum viable example of an Alumni Credential.",
  "issuer": "https://vc.example/issuers/5678",
  "validFrom": "2023-01-01T00:00:00Z",
  "credentialSubject": {
    "id": "did:example:abcdefgh",
    "alumniOf": "The School of Examples"
  },
  "proof": {
    "type": "DataIntegrityProof",
    "cryptosuite": "eddsa-jcs-2022",
    "created": "2023-02-24T23:36:38Z",
    "verificationMethod": "https://vc.example/issuers/5678#z6MkrJVnaZkeFzdQyMZu1cgjg7k1pZZ6pvBQ7XJPt4swbTQ2",
    "proofPurpose": "assertionMethod",
    "proofValue": "z3P6rHMUaWG6e3Ac6xYFht8aEvoVXndgKTtEY8kzWYXzk8dKmAo2GJeZiJw4qoZ2PGp4ugdaHx3oQiLpeFBLDqP2M"
  }
}';

    $verified = (new JcsEddsa2022())->verify(json_decode($document, true), $publicKey);
    $this->assertTrue($verified, 'Verify eddsa-jcs-2022 (from specification)');

    }

    public function testSignAndVerify()
    {
        $publicKey = 'z6MkfpucGTDbMZADwM6vEa8pS3s8Z9xqSEn6HihijZ4fVs9d';
        $channel = [
            'channel_id' => 1,
            'channel_url' => 'https://example.com/channel/klingon',
            'channel_epubkey' => 'FGdbYgr526Swuyya3e8epCBdHahlWNg9I0sBhMKCzpw',
            'channel_eprvkey' => 'StLRo8xb7VJ5XdR10OUYQM/uooP7D7fMlgvQFa1wrZIUZ1tiCvnbpLC7LJrd7x6kIF0dqGVY2D0jSwGEwoLOnA',
            'channel_address' => 'klingon@example.com',
            'channel_system' => false,
        ];

        $document = '{
    "@context": [
        "https://www.w3.org/ns/activitystreams",
        "https://w3id.org/security/v1",
        "https://www.w3.org/ns/did/v1",
        "https://w3id.org/security/multikey/v1",
        {
            "nomad": "https://example.com/apschema#",
            "toot": "http://joinmastodon.org/ns#",
            "litepub": "http://litepub.social/ns#",
            "manuallyApprovesFollowers": "as:manuallyApprovesFollowers",
            "oauthRegistrationEndpoint": "litepub:oauthRegistrationEndpoint",
            "sensitive": "as:sensitive",
            "movedTo": "as:movedTo",
            "discoverable": "toot:discoverable",
            "indexable": "toot:indexable",
            "capabilities": "litepub:capabilities",
            "acceptsJoins": "litepub:acceptsJoins",
            "Hashtag": "as:Hashtag",
            "canReply": "toot:canReply",
            "canSearch": "nomad:canSearch",
            "approval": "toot:approval",
            "expires": "nomad:expires",
            "directMessage": "nomad:directMessage",
            "Category": "nomad:Category",
            "copiedTo": "nomad:copiedTo",
            "searchContent": "nomad:searchContent",
            "searchTags": "nomad:searchTags"
        }
    ],
    "type": "Person",
    "id": "https://example.com/channel/klingon",
    "preferredUsername": "klingon",
    "name": "klingon",
    "created": "2023-07-13T20:23:32Z",
    "updated": "2023-07-13T20:23:32Z",
    "icon": {
        "type": "Image",
        "mediaType": "image/png",
        "updated": "2023-07-13T20:23:32Z",
        "url": "https://example.com/photo/profile/l/2",
        "height": 300,
        "width": 300
    },
    "url": "https://example.com/channel/klingon",
    "tag": [
        {
            "type": "Note",
            "name": "Protocol",
            "content": "zot6"
        },
        {
            "type": "Note",
            "name": "Protocol",
            "content": "nomad"
        },
        {
            "type": "Note",
            "name": "Protocol",
            "content": "activitypub"
        }
    ],
    "inbox": "https://example.com/inbox/klingon",
    "outbox": "https://example.com/outbox/klingon",
    "followers": "https://example.com/followers/klingon",
    "following": "https://example.com/following/klingon",
    "wall": "https://example.com/outbox/klingon",
    "endpoints": {
        "sharedInbox": "https://example.com/inbox",
        "oauthRegistrationEndpoint": "https://example.com/api/client/register",
        "oauthAuthorizationEndpoint": "https://example.com/authorize",
        "oauthTokenEndpoint": "https://example.com/token",
        "searchContent": "https://example.com/search/klingon?search={}",
        "searchTags": "https://example.com/search/klingon?tag={}"
    },
    "discoverable": true,
    "canSearch": [],
    "indexable": false,
    "publicKey": {
        "id": "https://example.com/channel/klingon?operation=rsakey",
        "owner": "https://example.com/channel/klingon",
        "signatureAlgorithm": "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256",
        "publicKeyPem": "-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA+LXyOD/bzzVgM/nUOJ5m
c4WrQPMlhKqWJvKrumdQw9JJYcyaZp/jmMxDx/w/EwVw+wnV5wZcD0yBVhC7NPRa
nYc5OfNhS4MO74xgZrj+VWSTzNo3YooS/dEIIvsu6bhxfooHj17SA6pMRnZkkVpk
ykpPRYwJw+NvKcRwzpF06rxMqjZ+Bp0ea/X37j4cHaosRoQTJiHmMKKnpByKdImF
TR1juJ69ASh6nh8YVGcz6fz1jBQZPMx05tfNdyN5oZRTr8Nug2CiF3V7yKKS14HD
kE9eeFeTMt58Qi+8kprATYxKrlIuTZmI4YdIRgtM+tPQsosKTFmjzbef4dYooutv
T7XfrE+wYVZlx2pkaeFiKrJVacpmmFJe8zCIFXrofq1aOagU1kpwnXgjneCttA+M
OJ3Y+cPamdfRQDtsBcokJUD40RTwux6OGW9zqkJIpniVB+CZu4nTOHCzMJwbxF0p
JmGZd9kc3PR6Uf/IHAb1xeyTi4FyyYTbRDYuJyqRKbe880QUwgCBcogIbNy4xxsH
UTMy0ucWaDSBRahKUIHl3FRglvnI754NJSXBDIQOwC9oRRH27Vmm1Jy8sltmFLFr
ENJCGgOH8Bhpk+y1jtw1jpTig76wIvw+6zQtgNSfPnrNGIHt5mcoy4pFFXLv2lK2
/u26hUGQAq71Ra0DwgXIWFECAwEAAQ==
-----END PUBLIC KEY-----
"
    },
    "assertionMethod": [
        {
            "id": "https://example.com/channel/klingon#z6MkfpucGTDbMZADwM6vEa8pS3s8Z9xqSEn6HihijZ4fVs9d",
            "type": "Multikey",
            "controller": "https://example.com/channel/klingon",
            "publicKeyMultibase": "z6MkfpucGTDbMZADwM6vEa8pS3s8Z9xqSEn6HihijZ4fVs9d"
        }
    ],
    "manuallyApprovesFollowers": true
}';

        $algorithm = new JcsEddsa2022();
        $documentArray = json_decode($document,true);
        $documentArray['proof'] = $algorithm->sign($documentArray, $channel);

        $verified = (new JcsEddsa2022())->verify($documentArray, $publicKey);
        $this->assertTrue($verified, 'Verify encode and decode eddsa-jcs-2022');

    }

    public function testkeyDiscovery()
    {
        $keyAsArray = '{
    "@context": [
        "https://www.w3.org/ns/activitystreams",
        "https://www.w3.org/ns/did/v1",
        "https://w3id.org/security/v1",
        "https://w3id.org/security/data-integrity/v1",
        "https://w3id.org/security/multikey/v1",
        {
            "manuallyApprovesFollowers": "as:manuallyApprovesFollowers",
            "proofValue": "sec:proofValue",
            "proofPurpose": "sec:proofPurpose",
            "VerifiableIdentityStatement": "mitra:VerifiableIdentityStatement",
            "mitra": "http://jsonld.mitra.social#",
            "schema": "http://schema.org/",
            "PropertyValue": "schema:PropertyValue",
            "toot": "http://joinmastodon.org/ns#",
            "sameAs": "schema:sameAs",
            "IdentityProof": "toot:IdentityProof",
            "subscribers": "mitra:subscribers",
            "MitraJcsEip191Signature2022": "mitra:MitraJcsEip191Signature2022",
            "value": "schema:value",
            "featured": "toot:featured"
        }
    ],
    "id": "https://public.mitra.social/users/macgirvin",
    "type": "Person",
    "name": null,
    "preferredUsername": "macgirvin",
    "inbox": "https://public.mitra.social/users/macgirvin/inbox",
    "outbox": "https://public.mitra.social/users/macgirvin/outbox",
    "followers": "https://public.mitra.social/users/macgirvin/followers",
    "following": "https://public.mitra.social/users/macgirvin/following",
    "subscribers": "https://public.mitra.social/users/macgirvin/subscribers",
    "featured": "https://public.mitra.social/users/macgirvin/collections/featured",
    "assertionMethod": [
        {
            "id": "https://public.mitra.social/users/macgirvin#main-key",
            "type": "Multikey",
            "controller": "https://public.mitra.social/users/macgirvin",
            "publicKeyMultibase": "z4MXj1wBzi9jUstyPhNdHCNq5zh1nnUyLvRMvQaNGHCpgauqPBxhKMEmpN99FWqk75qnCFyDPbCc2R14F52p4G4uN9CcZ3BoLc9SvyRM79714XNnoDHmpQGj4MkoMtKbHAVZ8ZuxsBkZGWU9k121tR1kV3qTwG1VHLXMK4stVYD5VRrAXdy1AXTUfLg13Ma2QqAmagEu8mYzDz1gUxn3LsVJ36uuZCwzgtGYNBmTnaydXdr4sYYV5qER4atP8ZGPkEwT4SW29RmumwCt8CAyxxqQ1CrUQnG2WKSD79L2AiXURRFyVgbrAESNh7AWDxtiTRuEQdC3gR4Y3jGhL76RsS7o6ez7LzWUUajyGrqP2iQLnTFFiXwua"
        },
        {
            "id": "https://public.mitra.social/users/macgirvin#ed25519-key",
            "type": "Multikey",
            "controller": "https://public.mitra.social/users/macgirvin",
            "publicKeyMultibase": "z6Mkndwt8kP3C5APJCeALaiy6xUog2idZKu6HBwirSw8sZae"
        }
    ],
    "authentication": [
        {
            "id": "https://public.mitra.social/users/macgirvin#main-key",
            "type": "Multikey",
            "controller": "https://public.mitra.social/users/macgirvin",
            "publicKeyMultibase": "z4MXj1wBzi9jUstyPhNdHCNq5zh1nnUyLvRMvQaNGHCpgauqPBxhKMEmpN99FWqk75qnCFyDPbCc2R14F52p4G4uN9CcZ3BoLc9SvyRM79714XNnoDHmpQGj4MkoMtKbHAVZ8ZuxsBkZGWU9k121tR1kV3qTwG1VHLXMK4stVYD5VRrAXdy1AXTUfLg13Ma2QqAmagEu8mYzDz1gUxn3LsVJ36uuZCwzgtGYNBmTnaydXdr4sYYV5qER4atP8ZGPkEwT4SW29RmumwCt8CAyxxqQ1CrUQnG2WKSD79L2AiXURRFyVgbrAESNh7AWDxtiTRuEQdC3gR4Y3jGhL76RsS7o6ez7LzWUUajyGrqP2iQLnTFFiXwua"
        },
        {
            "id": "https://public.mitra.social/users/macgirvin#ed25519-key",
            "type": "Multikey",
            "controller": "https://public.mitra.social/users/macgirvin",
            "publicKeyMultibase": "z6Mkndwt8kP3C5APJCeALaiy6xUog2idZKu6HBwirSw8sZae"
        }
    ],
    "publicKey": {
        "id": "https://public.mitra.social/users/macgirvin#main-key",
        "owner": "https://public.mitra.social/users/macgirvin",
        "publicKeyPem": "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwKYSGqal5/oOJ3tCtLwo\n/n4DvaKO/l87YY5ZrHQOEWWZWiolR4GNroBLPSNRhColkEfHwI36B4+VPQKI4VMj\n9eOF0Yz4r2DWXzzIhu4KS/nw+F6pwyelHKIL/tHhZtN0N37rRoZJ94kHIC9bZDg0\n0L4m38MwWQOsLzgcJu+fLBq/FsA5onmS6wTVBIj6c2CDJUP1tmqSKW8S5ayfbLiA\nQpSVa3Fo2AAmPshk7xXTmoNGuXaPpHw8InniKXmxYAsYrXR+4M1A3MGYbARd8j0y\ntb8Eb1slM7yx9xRQsI3U86myR53BZSqmt6hPwNt9Fl494GCe23Y34CPmSYyvLxA8\n5wIDAQAB\n-----END PUBLIC KEY-----\n"
    },
    "manuallyApprovesFollowers": false,
    "url": "https://public.mitra.social/users/macgirvin"
}';
        $key = Activity::getEddsaPublicKey(json_decode($keyAsArray,true));
        $this->assertEquals('z6Mkndwt8kP3C5APJCeALaiy6xUog2idZKu6HBwirSw8sZae', $key, 'discover key as array');

        $keyAsObject = '{
  "@context":[
    "https://www.w3.org/ns/activitystreams",
    "https://w3id.org/security/v1",
    "https://www.w3.org/ns/did/v1",
    "https://w3id.org/security/multikey/v1",
    "https://w3id.org/security/data-integrity/v1",
    "https://purl.archive.org/socialweb/webfinger",
    {
      "nomad":"https://rebble.net/apschema#",
      "toot":"http://joinmastodon.org/ns#",
      "manuallyApprovesFollowers":"as:manuallyApprovesFollowers",
      "oauthRegistrationEndpoint":"nomad:oauthRegistrationEndpoint",
      "sensitive":"as:sensitive",
      "movedTo":"as:movedTo",
      "discoverable":"toot:discoverable",
      "indexable":"toot:indexable",
      "Hashtag":"as:Hashtag",
      "canReply":"toot:canReply",
      "canSearch":"nomad:canSearch",
      "expires":"nomad:expires",
      "directMessage":"nomad:directMessage",
      "Category":"nomad:Category",
      "copiedTo":"nomad:copiedTo",
      "searchContent":"nomad:searchContent",
      "searchTags":"nomad:searchTags"
    }
  ],
  "id":"https://rebble.net/channel/slop",
  "type":"Person",
  "attachment":[

  ],
  "name":"slop",
  "icon":{
    "type":"Image",
    "mediaType":"image/png",
    "updated":"2023-03-03T09:31:54Z",
    "url":"https://rebble.net/photo/profile/l/14",
    "height":300,
    "width":300
  },
  "published":"2023-03-03T09:31:54Z",
  "tag":[
    {
      "type":"Note",
      "name":"Protocol",
      "content":"zot6"
    },
    {
      "type":"Note",
      "name":"Protocol",
      "content":"nomad"
    },
    {
      "type":"Note",
      "name":"Protocol",
      "content":"activitypub"
    }
  ],
  "updated":"2023-03-03T09:31:54Z",
  "url":"https://rebble.net/channel/slop",
  "canSearch":[

  ],
  "inbox":"https://rebble.net/inbox/slop",
  "outbox":"https://rebble.net/outbox/slop",
  "followers":"https://rebble.net/followers/slop",
  "following":"https://rebble.net/following/slop",
  "endpoints":{
    "sharedInbox":"https://rebble.net/inbox",
    "oauthRegistrationEndpoint":"https://rebble.net/api/client/register",
    "oauthAuthorizationEndpoint":"https://rebble.net/authorize",
    "oauthTokenEndpoint":"https://rebble.net/token",
    "searchContent":"https://rebble.net/search/slop?search={}",
    "searchTags":"https://rebble.net/search/slop?tag={}"
  },
  "publicKey":{
    "id":"https://rebble.net/channel/slop?operation=rsakey",
    "owner":"https://rebble.net/channel/slop",
    "signatureAlgorithm":"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256",
    "publicKeyPem":"-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAq0IyEAts7TLUgWMm4SgE\n4lzDLI8JVgfiaMKTuC4dEJ6wrbEBK53XfFV3tQHQo8EJEYM5IeTFPJ9ueqbw6YGx\nDiFbmLY3Wn0t8ETGjYSGx+iF8dWjMxFQ/e8/eiyNXi0JZJypTCiSISN0brgTkn0h\n5dTmevgSLYzPoumjuY2a0XbDl48kVyRuvNlNhFGnQVypFhuoJvKlpw9jqvlUJD93\nbF2RMQ1UotKJZUCBV9BioZMEQu9JXZZWNAYdDtqzbB/thBDhFDYjA264LBUj87Pm\nW/cIgUn7oAVcejfUHp+aTpOjDsFNrf6OJwvHcMO2brNcPZuFr7QPg9egQHzA4aGS\nyn+jJR5sKotZEPMzP8xPpoURrQReuk18pIFB2RJzZzLUKOZ6f8LVhS39QxBEptGI\ncaa+WtEtNcDWzh7jVceijdKxB0m83oXA5LABIEK5Ullu05IxJLFdup9p8DIdrmek\nsZ+Vd9603ssWsz9g80GuaqdrIaZx8FC6E4G7KSknh1okPsH1v4p7g5GXhcQVAvW9\nWKA3N5Su6TyxMJOA3U1Ql4NDN/dZdtHzgMc9fZrV4V2a1bwtFf9MR7/GvlyEFzYB\nup+UZKao1OSd7HmmeADUGH+TK1hO4iHxo4JKV7gGw6wU2f78Ffv/L8xAF2W4BOhe\nsaFNzs+OC2xQz6KYKeaEowcCAwEAAQ==\n-----END PUBLIC KEY-----\n"
  },
  "preferredUsername":"slop",
  "discoverable":true,
  "manuallyApprovesFollowers":true,
  "webfinger":"acct:slop@rebble.net",
  "indexable":false,
  "assertionMethod":[
    {
      "id":"https://rebble.net/channel/slop#z6MkrD9t4uWqskmcVZVyzHVnRUVTnEM4fTCydm7oWMBXUHQH",
      "type":"Multikey",
      "controller":"https://rebble.net/channel/slop",
      "publicKeyMultibase":"z6MkrD9t4uWqskmcVZVyzHVnRUVTnEM4fTCydm7oWMBXUHQH"
    }
  ],
  "proof":{
    "type":"DataIntegrityProof",
    "cryptosuite":"eddsa-jcs-2022",
    "created":"2024-03-07T21:04:19Z",
    "verificationMethod":"https://rebble.net/channel/slop#z6MkrD9t4uWqskmcVZVyzHVnRUVTnEM4fTCydm7oWMBXUHQH",
    "proofPurpose":"assertionMethod",
    "proofValue":"z3zFy4QaAiV2mUnt8fyy2thvanDKLdCQEWgCrAmCtPVYhov1jxLBKymxBaVwzvz9da8pEh1TWcJMp9hdsSpj53pBT"
  }
}';
        $key = Activity::getEddsaPublicKey(json_decode($keyAsObject,true));
        $this->assertEquals('z6MkrD9t4uWqskmcVZVyzHVnRUVTnEM4fTCydm7oWMBXUHQH', $key, 'discover key as array');

    }
}
