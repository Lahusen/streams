<?php

namespace Code\Update;

class _1269
{
   public function run()
   {
       $channels = q("select * from channel where true");
       if ($channels) {
           foreach ($channels as $channel) {
               if ($channel['channel_allow_gid']) {
                   $channel['channel_allow_gid'] = '<' . 'connections:' . $channel['channel_hash'] . '>';
                   q("update channel set channel_allow_gid = '%s' where channel_id = %d",
                       dbesc($channel['channel_allow_gid']),
                       (int)$channel['channel_id']
                   );
               }
           }
       }
       return UPDATE_SUCCESS;
   }

   public function verify()
   {
       return true;
   }
}
