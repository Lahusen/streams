<?php

namespace Code\Update;

class _1272
{
    public function run()
    {

        q("START TRANSACTION");

        if (ACTIVE_DBTYPE == DBTYPE_POSTGRES) {
            $r1 = q("ALTER TABLE outq ADD outq_mid text NOT NULL DEFAULT '' ");
            $r2 = q("create index \"outq_mid_idx\" on outq (\"outq_mid\")");

            $r = ($r1 && $r2);
        } else {
            $r = q("ALTER TABLE `outq` ADD `outq_mid` varchar(255) NOT NULL DEFAULT '' , 
				ADD INDEX `outq_mid` (`outq_mid`)");
        }

        if ($r) {
            q("COMMIT");
            return UPDATE_SUCCESS;
        }

        q("ROLLBACK");
        return UPDATE_FAILED;
    }

    public function verify()
    {

        $columns = db_columns('outq');

        if (in_array('outq_mid', $columns)) {
            return true;
        }

        return false;
    }

}