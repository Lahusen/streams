<?php

namespace Code\Update;

use Code\Lib\Channel;
use Code\Lib\PConfig;


class _1283
{
    public function run()
    {
        $channels = q("select * from channel left join xchan on channel_hash = xchan_hash and xchan_network like ('%s')",
            dbesc('nomad%')
        );
        if ($channels) {
            foreach ($channels as $channel) {
                $nomadic = (PConfig::Get($channel['channel_id'], 'system', 'nomadicAP') || $channel['xchan_network'] === 'nomadic');
                if ($nomadic) {
                    q("UPDATE xchan set xchan_url = '%s' where xchan_id = %d",
                        dbesc(Channel::getDidResolver($channel, true)),
                        intval($channel['xchan_id'])
                    );
                    q("UPDATE hubloc set hubloc_id_url = '%s' where hubloc_hash = '%s'",
                        dbesc(Channel::getDidResolver($channel, true)),
                        dbesc($channel['xchan_hash'])
                    );
                }
            }
        }
        return UPDATE_SUCCESS;
    }

    public function verify()
    {
        return true;
    }



}