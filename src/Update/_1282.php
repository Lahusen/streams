<?php

namespace Code\Update;

class _1282
{
    public function run()
    {
        q("START TRANSACTION");

        if (ACTIVE_DBTYPE == DBTYPE_POSTGRES) {
            $r1 = q('CREATE TABLE "locator" (
              "id" serial NOT NULL,
              "locate_old" text NOT NULL,
              "locate_new" text NOT NULL,
              PRIMARY KEY ("id"),
              UNIQUE ("locate_old"))');
            $r2 = q('create index "locate_new_idx" on locator ("locate_new")');
            $r = ($r1 && $r2);
        }
        else {
            $r = q("CREATE TABLE IF NOT EXISTS locator (
                id int NOT NULL AUTO_INCREMENT,
                locate_old varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
                locate_new varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
                PRIMARY KEY (id),
                UNIQUE KEY locate_old (locate_old(191)),
                KEY locate_new (locate_new(191))
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4");
        }

        if ($r) {
            q("COMMIT");
            return UPDATE_SUCCESS;
        }

        q("ROLLBACK");
        return UPDATE_FAILED;
    }

    public function verify()
    {
        $columns = db_columns('locator');

        if ($columns && in_array('locate_old', $columns)) {
            return true;
        }
        return false;
    }



}