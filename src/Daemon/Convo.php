<?php

namespace Code\Daemon;

use Code\Lib\Activity;
use Code\Lib\ASCollection;
use Code\Lib\Channel;

/*
 * Given an id of a collection of messages (such as a replies collection or context),
 * fetch and store all the conversation children. At this time we will not attempt to recurse
 * beyond what is provided in the immediate collection.
 */

class Convo implements DaemonInterface
{

    /**
     * @param int $argc
     * @param array $argv
     * @return void
     */
    public function run(int $argc, array $argv): void
    {
        logger('convo invoked: ' . print_r($argv, true));

        if ($argc != 4) {
            killme();
        }

        $id = $argv[1];
        $channel_id = intval($argv[2]);
        $contact_hash = $argv[3];

        $channel = Channel::from_id($channel_id);
        if (! $channel) {
            killme();
        }

        $collection = new ASCollection($id, $channel);

        $messages = $collection->fetchAndParse(children_only: true);

        if ($messages) {
            foreach ($messages as $message) {
                Activity::store($channel, $contact_hash, $message['activity'], $message['item'], false, false, $message['collection']);
            }
        }
    }
}
