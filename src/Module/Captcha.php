<?php

namespace Code\Module;

use Code\Web\Controller;
use Gregwar\Captcha\CaptchaBuilder;

class Captcha extends Controller
{

    public function init()
    {
        $captcha = new CaptchaBuilder;
        $_SESSION['phrase'] = $captcha->getPhrase();

        header('Content-Type: image/jpeg');

        $captcha
            ->build()
            ->output()
        ;
        killme();
    }
}