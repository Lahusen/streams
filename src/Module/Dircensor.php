<?php

namespace Code\Module;

use Code\Daemon\Run;
use Code\Web\Controller;

class Dircensor extends Controller
{

    public function get()
    {
        if (!is_site_admin()) {
            return;
        }

        $xchan = hex2bin(argv(1));
        if (!$xchan) {
            return;
        }

        $r = q(
            "select * from xchan where xchan_hash = '%s'",
            dbesc($xchan)
        );

        if (!$r) {
            return;
        }

        $val = ($r[0]['xchan_censored']) ? 0 : 2;

        q(
            "update xchan set xchan_censored = $val where xchan_hash = '%s'",
            dbesc($xchan)
        );

        if ($val) {
            Run::Summon(['Xchan_photo', bin2hex($r[0]['xchan_photo_l']), bin2hex($xchan)]);
            info(t('Entry censored') . EOL);
        } else {
            info(t('Entry uncensored') . EOL);
        }

        goaway(z_root() . '/directory');
    }
}
