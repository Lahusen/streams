<?php

namespace Code\Module;

use App;
use Code\Lib\AccessControl;
use Code\Lib\AccessList;
use Code\Lib\Libsync;
use Code\Web\Controller;

class Audience extends Controller
{
    public function post()
    {
        if (!local_channel()) {
            return;
        }
        $channel = App::get_channel();
        $channel_allow_gid = perms2str($_REQUEST['group_allow']);
        q("update channel set channel_allow_gid = '%s' where channel_id = %d",
            dbesc($channel_allow_gid),
            intval($channel['channel_id'])
        );
        App::$channel['channel_allow_gid'] = $channel_allow_gid;
        Libsync::build_sync_packet();
    }

    public function get()
    {
        if (!local_channel()) {
            return login();
        }
        return AccessList::AudienceSelect();
    }


}
