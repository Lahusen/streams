<?php
namespace Code\Module;

use App;
use Code\Web\Controller;
use Code\Lib\System;
use Code\Render\Theme;                                                                                                                                            
use Code\Lib\Channel;


class Opensearch extends Controller {

	function init() {

		// stock project icons are 64px which is required for this interface
		// custom site icons will normally be [ 300,80,48 ] px.
		// Detect which one of these is in play and rewrite custom icon urls
		// so to generate a 64px icon on demand
		
		$icon = System::get_site_icon();
        $channel = null;

		if (!str_contains($icon, z_root() . '/image')) {
			$icon = str_replace('/m/','/64/',$icon);
		}

        if (argc() > 1) {
            $channel = Channel::from_username(argv(1));
            if (!$channel) {
                killme();
            }
        }

        header("Content-type: application/opensearchdescription+xml");
        if($channel) {
            $html = replace_macros(Theme::get_template('opensearch.tpl'), [
                '$project' => xmlify($channel['channel_address'] . '@' . App::get_hostname()),
                '$search_project' => xmlify(t('Search $Projectname') . '|' . $channel['channel_address'] . '@' . App::get_hostname()),
                '$searchurl' => xmlify(zid(z_root() . '/search/' . $channel['channel_address'] . '?search={searchTerms}')),
                '$aptype' => xmlify('application/ld+json; profile="https://www.w3.org/ns/activitystreams"'),
                '$photo' => xmlify($channel['xchan_photo_s']),
            ]);
        }
        else {
            $html = replace_macros(Theme::get_template('opensearch.tpl'), [
                '$project' => xmlify(App::get_hostname()),
                '$search_project' => xmlify(t('Search $Projectname') . '|' . App::get_hostname()),
                '$searchurl' => xmlify(zid(z_root() . '/search?search={searchTerms}')),
                '$aptype' => xmlify('application/ld+json; profile="https://www.w3.org/ns/activitystreams"'),
                '$photo' => xmlify($icon),
            ]);
        }
        echo $html;
		killme();
	}
}
