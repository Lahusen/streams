<?php

namespace Code\Module;

use Code\Web\Controller;

class Privacy_policy extends Controller
{
    public function init()
    {
        goaway('/legal');
    }

}