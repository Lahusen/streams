<?php

namespace Code\Module;

use Code\Web\Controller;
use Code\Lib\ActivityStreams;
use Code\Lib\Activity as ZlibActivity;
use Code\Lib\Libzot;
use Code\Web\HTTPSig;
use Code\Lib\ThreadListener;
use Code\Lib\Channel;
use App;

class Replies extends Controller
{

    public function init()
    {
        if (ActivityStreams::is_as_request() || Libzot::is_nomad_request()) {
            $item_mid = argv(1);

            if (!$item_mid) {
                http_status_exit(404, 'Not found');
            }

            $portable_id = EMPTY_STR;

            $item_normal = " and item.item_hidden = 0 and item.item_type = 0 and item.item_unpublished = 0 and item.item_delayed = 0 and item.item_blocked = 0 and not verb in ( 'Follow', 'Ignore' ) ";

            $i = null;

            // do we have the item (at all)?

            $test = q("select * from item where mid like '%s' $item_normal limit 1",
                dbesc(z_root() . protect_sprintf('/%/') . $item_mid)
            );

            if (!$test) {
                http_status_exit(404, 'Not found');
            }
            // process an authenticated fetch
            $sigdata = HTTPSig::verify(EMPTY_STR);
            if ($sigdata['portable_id'] && $sigdata['header_valid']) {
                $portable_id = $sigdata['portable_id'];
                observer_auth($portable_id);

                // first see if we have a copy of this item owned by the current signer
                // include xchans for all zot-like networks - these will have the same guid and public key

                $x = q(
                    "select * from xchan where xchan_hash = '%s'",
                    dbesc($sigdata['portable_id'])
                );

                if ($x) {
                    $xchans = q(
                        "select xchan_hash from xchan where xchan_hash = '%s' OR ( xchan_guid = '%s' AND xchan_pubkey = '%s' ) ",
                        dbesc($sigdata['portable_id']),
                        dbesc($x[0]['xchan_guid']),
                        dbesc($x[0]['xchan_pubkey'])
                    );

                    if ($xchans) {
                        $hashes = ids_to_querystr($xchans, 'xchan_hash', true);
                        $i = q(
                            "select item.*, id as item_id from item where mid = '%s' $item_normal and owner_xchan in ( " . protect_sprintf($hashes) . " ) limit 1",
                            dbesc($test[0]['mid'])
                        );
                    }
                }
            }

            // if we don't have a parent id belonging to the signer see if we can obtain one as a visitor that we have permission to access
            // with a bias towards those items owned by channels on this site (item_wall = 1)

            $sql_extra = item_permissions_sql(0);

            if (!$i) {
                $i = q(
                    "select item.*, id as item_id from item where mid = '%s' $item_normal $sql_extra order by item_wall desc limit 1",
                    dbesc($test[0]['mid'])
                );
            }

            if (!$i) {
                http_status_exit(403, 'Forbidden');
            }

            xchan_query($i);

            $sql_noAdd = " and verb not in ('Add', 'Remove') ";

            $items = q(
                "SELECT item.*, item.id AS item_id FROM item WHERE item.thr_parent = '%s' and item.mid != item.parent_mid $item_normal $sql_noAdd and item_private = %d and item_deleted = 0 and uid = %d",
                dbesc($i[0]['mid']),
                intval($i[0]['item_private']),
                intval($i[0]['uid'])
            );

            if (!$items) {
                http_status_exit(404, 'Not found');
            }

            xchan_query($items, true);
            $items = fetch_post_tags($items);

            $observer = App::get_observer();
            $parent = $i[0];
            $recips = ((in_array($parent['owner']['xchan_network'], ['activitypub', 'apnomadic'])) ? get_iconfig($parent['id'], 'activitypub', 'recips', []) : []);
            $to = (($recips && array_key_exists('to', $recips) && is_array($recips['to'])) ? $recips['to'] : null);
            $nitems = [];
            foreach ($items as $item) {
                $mids = [];

                if (intval($item['item_private'])) {
                    if (!$observer) {
                        continue;
                    }
                    // ignore private reshare, possibly from hubzilla
                    if ($i['verb'] === 'Announce') {
                        if (!in_array($item['thr_parent'], $mids)) {
                            $mids[] = $item['thr_parent'];
                        }
                        continue;
                    }
                    // also ignore any children of the private reshares
                    if (in_array($item['thr_parent'], $mids)) {
                        continue;
                    }
                    
                    if ($observer['xchan_hash'] !== $item['owner_xchan']) {
                        if (empty($to)) {
                            continue;
                        }
                        if (!in_array($observer['xchan_url'], $to)) {
                            continue;
                        }
                    }
                }
                $nitems[] = $item;
            }

            $chan = Channel::from_id($i[0]['uid']);

            if (!$chan) {
                http_status_exit(404, 'Not found');
            }

            if (!perm_is_allowed($chan['channel_id'], get_observer_hash(), 'view_stream')) {
                http_status_exit(403, 'Forbidden');
            }

            $collection = ZlibActivity::encode_item_collection($nitems, 'replies/' . $item_mid, 'OrderedCollection', true, $chan, count($nitems));

            if (!$collection) {
                http_status_exit(404, 'Not found');
            }

            $channel = Channel::from_id($i[0]['uid']);
            as_return_and_die($collection, $channel);
        }

        goaway(z_root() . '/item/' . argv(1));
    }
}
