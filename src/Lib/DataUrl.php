<?php

namespace Code\Lib;

class DataUrl
{
    protected $data = null;
    protected $mediaType = null;
    protected $encoding = null;

    public function __construct($data, $mediaType = '', $encoding = '')
    {
        $this->data = $data;
        $this->mediaType = $mediaType;
        $this->encoding = $encoding;
        return $this;
    }

    public function encode()
    {
        return 'data:' . $this->mediaType . (($this->encoding) ? ';' . $this->encoding : '') . ','
            . (($this->encoding) ? base64_encode($this->data) : urlencode($this->data));

    }

    public function decode()
    {
        if (str_starts_with($this->data, 'data:')) {
            $explode = explode(',', $this->data);
            if (count($explode) === 2) {
                $this->mediaType = substr($explode[0], strlen('data:'));
                if (str_ends_with($this->mediaType, ';base64')) {
                    $this->encoding = 'base64';
                    $this->mediaType = substr($this->mediaType, 0, strlen($this->mediaType) - strlen(';base64'));
                } else {
                    $this->encoding = '';
                }
                return [
                    'data' => $this->encoding ? base64_decode($this->data) : urldecode($this->data),
                    'encoding' => $this->encoding,
                    'mediaType' => $this->mediaType ?: 'text/plain;charset=US-ASCII',
                ];
            }
        }
        return null;
    }

}
