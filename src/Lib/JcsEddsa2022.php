<?php

namespace Code\Lib;

use Root23\JsonCanonicalizer\JsonCanonicalizer;
use StephenHill\Base58;


class JcsEddsa2022
{

    public  function  __construct()
    {
        return $this;
    }

    public function sign($data, $channel): array
    {
        if (!$channel) {
            return [];
        }

        unset($data['proof']); // Discard any pre-existing signature if it exists

        $options = [
            'type' => 'DataIntegrityProof',
            'cryptosuite' => 'eddsa-jcs-2022',
            'created' => Time::convert(format: ISO8601),
            'verificationMethod' => Channel::getVerifier($channel, $data['id'] ?? ''),
            'proofPurpose' => 'assertionMethod',
        ];

        $optionsHash = $this->hash($this->signableOptions($options), true);
        $dataHash = $this->hash($this->signableData($data), true);

        logger('signing_data: ' . print_r($this->signableOptions($options), true) . print_r($this->signableData($data),true), LOGGER_DATA);
        $options['proofValue'] = 'z' . (new Base58())->encode(sodium_crypto_sign_detached(
        $optionsHash . $dataHash,
                sodium_base642bin($channel['channel_eprvkey'], SODIUM_BASE64_VARIANT_ORIGINAL_NO_PADDING)));

        return $options;
    }

    public function verify($data, $publicKey)
    {
        $encodedSignature = $data['proof']['proofValue'] ?? '';
        if (!str_starts_with($encodedSignature,'z')) {
            return false;
        }

        $encodedSignature = substr($encodedSignature, 1);
        $optionsHash = $this->hash($this->signableOptions($data['proof']), true);
        $dataHash = $this->hash($this->signableData($data),true);

        logger('verifying_data: ' . print_r($this->signableOptions($data['proof']), true) . print_r($this->signableData($data),true), LOGGER_DATA);

        try {
            $result = sodium_crypto_sign_verify_detached((new Base58())->decode($encodedSignature),
        $optionsHash . $dataHash,
                (new Multibase())->decode($publicKey, true));
        }
        catch (\Exception $e) {
            // logger('publickey for exception: ' . $publicKey);
            logger('verify exception:' .  $e->getMessage());
        }

        logger('SignatureVerify (eddsa-jcs-2022) ' . (($result) ? 'true' : 'false'));

        return $result;
    }

    public function signableData($data)
    {
        $signableData = [];
        if ($data) {
            foreach ($data as $k => $v) {
                if ($k != 'proof') {
                    $signableData[$k] = $v;
                }
            }
        }
        return $signableData;
    }

    public function signableOptions($options)
    {
        $signableOptions = [];

        if ($options) {
            foreach ($options as $k => $v) {
                if ($k !== 'proofValue') {
                    $signableOptions[$k] = $v;
                }
            }
        }
        return $signableOptions;
    }

    public function hash($obj, $binary = false)
    {
        return hash('sha256', $this->canonicalize($obj), $binary);
    }

    public function canonicalize($data)
    {
        $canonicalizer = new JsonCanonicalizer();
        return $canonicalizer->canonicalize($data);
    }

}
