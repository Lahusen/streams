<?php

namespace Code\Lib;

use DateTime;
use DateTimeZone;
use Exception;

class Time
{
    public static function convert($from = 'UTC', $to = 'UTC', $datetime = 'now', $format = "Y-m-d H:i:s")
    {

        // Defaults to UTC if nothing is set, but throws an exception if set to empty string.
        // Provide some sane defaults regardless.

        if ($from === '') {
            $from = 'UTC';
        }
        if ($to === '') {
            $to = 'UTC';
        }
        if (($datetime === '') || (! is_string($datetime))) {
            $datetime = 'now';
        }

        if (in_array($datetime, ['0000-00-00 00:00:00', '0001-01-01 00:00:00'])) {
            $d = new DateTime('0001-01-01 00:00:00', new DateTimeZone('UTC'));
            return $d->format($format);
        }

        try {
            $from_obj = new DateTimeZone($from);
        } catch (Exception $e) {
            $from_obj = new DateTimeZone('UTC');
        }

        try {
            $d = new DateTime($datetime, $from_obj);
        } catch (Exception $e) {
            logger('exception: ' . $e->getMessage());
            $d = new DateTime('now', $from_obj);
        }

        try {
            $to_obj = new DateTimeZone($to);
        } catch (Exception $e) {
            $to_obj = new DateTimeZone('UTC');
        }

        $d->setTimeZone($to_obj);

        return($d->format($format));
    }
}
