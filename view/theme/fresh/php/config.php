<?php

namespace Code\Theme;

use App;
use Code\Lib\Features;
use Code\Render\Theme;


class freshConfig
{

    function get_schemas()
    {
        $files = glob('view/theme/fresh/schema/*.php');

        $scheme_choices = [];

        if ($files) {

            if (in_array('view/theme/fresh/schema/default.php', $files)) {
                $scheme_choices['---'] = t('Default');
                $scheme_choices['light'] = t('Light (default)');
            } else {
                $scheme_choices['---'] = t('Light (default)');
            }

            foreach ($files as $file) {
                $f = basename($file, ".php");
                if ($f != 'default') {
                    $scheme_name = $f;
                    $scheme_choices[$f] = $scheme_name;
                }
            }
        }

        return $scheme_choices;
    }

    function get()
    {
        if (!local_channel()) {
            return '';
        }

        $arr = [];
//        $arr['narrow_navbar'] = get_pconfig(local_channel(), 'fresh', 'narrow_navbar');
        $arr['link_color'] = get_pconfig(local_channel(), 'fresh', 'link_color');
        $arr['link_hover'] = get_pconfig(local_channel(), 'fresh', 'link_hover');
        $arr['banner_color'] = get_pconfig(local_channel(), 'fresh', 'banner_color');
        $arr['bgcolor'] = get_pconfig(local_channel(), 'fresh', 'background_color');
        $arr['overlaybg'] = get_pconfig(local_channel(), 'fresh', 'overlay_background_color');
        $arr['subtle_shade'] = get_pconfig(local_channel(), 'fresh', 'subtle_shade');
        $arr['mid_shade'] = get_pconfig(local_channel(), 'fresh', 'mid_shade');
        $arr['contrast_shade'] = get_pconfig(local_channel(), 'fresh', 'contrast_shade');
        $arr['background_image'] = get_pconfig(local_channel(), 'fresh', 'background_image');
        $arr['font_size'] = get_pconfig(local_channel(), 'fresh', 'font_size');
        $arr['font_color'] = get_pconfig(local_channel(), 'fresh', 'font_color');
        $arr['font_contrast_color'] = get_pconfig(local_channel(), 'fresh', 'font_contrast_color');
        $arr['primary'] = get_pconfig(local_channel(), 'fresh', 'primary_color');
        $arr['secondary'] = get_pconfig(local_channel(), 'fresh', 'secondary_color');
        $arr['action'] = get_pconfig(local_channel(), 'fresh', 'action_color');
        $arr['success'] = get_pconfig(local_channel(), 'fresh', 'success_color');
        $arr['warning'] = get_pconfig(local_channel(), 'fresh', 'warning_color');
        $arr['danger'] = get_pconfig(local_channel(), 'fresh', 'danger_color');
        $arr['border'] = get_pconfig(local_channel(), 'fresh', 'border');
        $arr['radius'] = get_pconfig(local_channel(), 'fresh', 'radius');
        $arr['shadow'] = get_pconfig(local_channel(), 'fresh', 'photo_shadow');
        $arr['converse_width'] = get_pconfig(local_channel(), "fresh", "converse_width");
        $arr['top_photo'] = get_pconfig(local_channel(), "fresh", "top_photo");
        $arr['reply_photo'] = get_pconfig(local_channel(), "fresh", "reply_photo");
        return $this->form($arr);
    }

    function post()
    {
        if (!local_channel()) {
            return;
        }

        if (isset($_POST['fresh-settings-submit'])) {
//            set_pconfig(local_channel(), 'fresh', 'narrow_navbar', $_POST['fresh_narrow_navbar']);
            set_pconfig(local_channel(), 'fresh', 'link_color', $_POST['fresh_link_color']);
            set_pconfig(local_channel(), 'fresh', 'link_hover', $_POST['fresh_link_hover']);
            set_pconfig(local_channel(), 'fresh', 'background_color', $_POST['fresh_background_color']);
            set_pconfig(local_channel(), 'fresh', 'overlay_background_color', $_POST['fresh_overlay_background_color']);
            set_pconfig(local_channel(), 'fresh', 'subtle_shade', $_POST['fresh_subtle_shade']);
            set_pconfig(local_channel(), 'fresh', 'mid_shade', $_POST['fresh_mid_shade']);
            set_pconfig(local_channel(), 'fresh', 'contrast_shade', $_POST['fresh_contrast_shade']);
            set_pconfig(local_channel(), 'fresh', 'banner_color', $_POST['fresh_banner_color']);
            set_pconfig(local_channel(), 'fresh', 'background_image', $_POST['fresh_background_image']);
            set_pconfig(local_channel(), 'fresh', 'font_size', $_POST['fresh_font_size']);
            set_pconfig(local_channel(), 'fresh', 'font_color', $_POST['fresh_font_color']);
            set_pconfig(local_channel(), 'fresh', 'font_contrast_color', $_POST['fresh_font_contrast_color']);
            set_pconfig(local_channel(), 'fresh', 'primary_color', $_POST['fresh_primary_color']);
            set_pconfig(local_channel(), 'fresh', 'secondary_color', $_POST['fresh_secondary_color']);
            set_pconfig(local_channel(), 'fresh', 'action_color', $_POST['fresh_action_color']);
            set_pconfig(local_channel(), 'fresh', 'success_color', $_POST['fresh_success_color']);
            set_pconfig(local_channel(), 'fresh', 'warning_color', $_POST['fresh_warning_color']);
            set_pconfig(local_channel(), 'fresh', 'danger_color', $_POST['fresh_danger_color']);
            set_pconfig(local_channel(), 'fresh', 'border', $_POST['fresh_border']);
            set_pconfig(local_channel(), 'fresh', 'radius', $_POST['fresh_radius']);
            set_pconfig(local_channel(), 'fresh', 'photo_shadow', $_POST['fresh_shadow']);
            set_pconfig(local_channel(), 'fresh', 'converse_width', $_POST['fresh_converse_width']);
            set_pconfig(local_channel(), 'fresh', 'top_photo', $_POST['fresh_top_photo']);
            set_pconfig(local_channel(), 'fresh', 'reply_photo', $_POST['fresh_reply_photo']);
        }
    }

    function form($arr)
    {

        $expert = Features::enabled(local_channel(), 'advanced_theming');

        return replace_macros(Theme::get_template('theme_settings.tpl'), array(
            '$submit' => t('Submit'),
            '$baseurl' => z_root(),
            '$theme' => App::$channel['channel_theme'],
            '$expert' => $expert,
            '$title' => t("Theme settings"),
//            '$narrow_navbar' => ['fresh_narrow_navbar', t('Narrow navbar'), $arr['narrow_navbar'], '', [t('No'), t('Yes')]],
            '$link_color' => ['fresh_link_color', t('Link color'), $arr['link_color']],
            '$link_hover' => ['fresh_link_hover', t('Link hover'), $arr['link_hover']],
            '$banner_color' => ['fresh_banner_color', t('Set font-color for banner'), $arr['banner_color']],
            '$bgcolor' => ['fresh_background_color', t('Set the background color'), $arr['bgcolor']],
            '$overlaybg' => ['fresh_overlay_background_color', t('Set the overlay background color'), $arr['overlaybg']],
            '$subtle_shade' => ['fresh_subtle_shade', t('Set the subtle shade'), $arr['subtle_shade']],
            '$mid_shade' => ['fresh_mid_shade', t('Set the middle-contrast shade'), $arr['mid_shade']],
            '$contrast_shade' => ['fresh_contrast_shade', t('Set the contrasting shade'), $arr['contrast_shade']],
            '$background_image' => ['fresh_background_image', t('Set the background image'), $arr['background_image']],
            '$font_size' => ['fresh_font_size', t('Set font-size for the entire application'), $arr['font_size'], t('Examples: 1rem, 100%, 16px')],
            '$font_color' => ['fresh_font_color', t('Set font-color for posts and comments'), $arr['font_color']],
            '$font_contrast_color' => ['fresh_font_contrast_color', t('Set the font color to work with the contrasting shade'), $arr['font_contrast_color']],
            '$primary' => ['fresh_primary_color', t('Set the primary color'), $arr['primary']],
            '$secondary' => ['fresh_secondary_color', t('Set the secondary color'), $arr['secondary']],
            '$action' => ['fresh_action_color', t('Set the action color'), $arr['action']],
            '$success' => ['fresh_success_color', t('Set the success color'), $arr['success']],
            '$warning' => ['fresh_warning_color', t('Set the warning color'), $arr['warning']],
            '$danger' => ['fresh_danger_color', t('Set the danger color'), $arr['danger']],
            '$border' => ['fresh_border', t('Set the element borders'), $arr['border'], t('Example: 1px solid #ccc')],
            '$radius' => ['fresh_radius', t('Set radius of corners'), $arr['radius'], t('Example: 3px')],
            '$shadow' => ['fresh_shadow', t('Set shadow depth of photos'), $arr['shadow']],
            '$converse_width' => ['fresh_converse_width', t('Set maximum width of content region in pixel'), $arr['converse_width'], t('Leave empty for default width')],
            '$top_photo' => ['fresh_top_photo', t('Set size of conversation author photo'), $arr['top_photo']],
            '$reply_photo' => ['fresh_reply_photo', t('Set size of followup author photos'), $arr['reply_photo']],
        ));
    }
}

