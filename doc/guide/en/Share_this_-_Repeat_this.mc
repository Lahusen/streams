Share or Repeat Posts/Comments
==============================

Suppose you see someone's post or comment, and you want your connections to see it too. You have two ways to do this (if the author allows it).

Look in the ⚙ dropdown menu for the item. You'll find two options: "Share this" and "Repeat this".

### Share this

This option creates a brand-new post, under your channel's name. It includes the post you are sharing, plus any material that you want to add.

"Share this" will be familiar if you've used Facebook's "Share" feature. Other platforms refer to it as "Quote-Post", "Quote-Tweet", "Retweet with Comment", "Reshare", "Reblog", or "Attach".

Since you are the owner of the new post, if anyone adds a comment, it will appear only on *your* post. Comments are not sent back to the original author of the material that you've shared.

You can set the audience for your new post, to control who can see it and who can comment.

**When should I use "Share this"?**
In any situation where you want to start a new conversation about someone else's post. For example:
* The original post already has hundreds of comments, and you want to talk about it with people you know.
* You want to take the discussion in a different direction, and not derail the discussion attached to the original post.
* You have personal stuff to say about the original post, and you only want to say it to a few of your connections.
* You want the original post to appear on your own channel's home page.

**Disadvantages of "Share this"**
* You are talking about someone's post behind their back (unless you include them in your post's audience). Some people may find this offensive.
* Does not provide the ability for your audience to interact with polls and surveys. 

### Repeat this

This option alerts your connections to the *original* post. Other platforms call this feature "Boost", "Repost", or "Announce". The post is repeated in its original form, and you can't add any of your own words to it, or restrict the people who can see it.

Any comments (made by you or others) will become part of the conversation attached to the original post.

**When should I use "Repeat this"?**
Whenever you want to draw attention to somebody else's post and its comments, without creating a new post of your own. For example:
* Somebody is asking for assistance, and you want to spread the word to your own connections.
* You want to help publicize an announcement, a news item, or a pretty picture.
* You want to share a poll or survey. 

**Disadvantages of "Repeat this"**
* The original post will not appear on your own channel's home page.
* You can only add your own thoughts as a comment on the original post.

